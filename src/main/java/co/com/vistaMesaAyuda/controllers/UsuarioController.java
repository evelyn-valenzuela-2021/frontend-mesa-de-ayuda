package co.com.vistaMesaAyuda.controllers;

import co.com.vistaMesaAyuda.dtos.Empleado;
import co.com.vistaMesaAyuda.dtos.Usuario;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

import java.net.URI;

import static org.springframework.http.HttpMethod.GET;

@Controller
@SessionAttributes("usuario")
public class UsuarioController {

    private final ModelMapper modelMapper;
    private final RestTemplate restTemplate;
    private final String url = "http://localhost:57086/api/usuarios";

    public UsuarioController(ModelMapper modelMapper, RestTemplate restTemplate) {
        this.modelMapper = modelMapper;
        this.restTemplate = restTemplate;
    }

    @ModelAttribute
    public void rutas(Model model) {
        model.addAttribute("ruta", "/index/login");
        model.addAttribute("nombreRuta", "Iniciar sesión");
        model.addAttribute("ruta2", "/index");
        model.addAttribute("nombreRuta2", "Inicio");
    }

    @GetMapping("/index")
    public String index(Model model) {
        rutas(model);
        return "unauthenticated/index";
    }

    @GetMapping("/index/login")
    public String login(Model model) {
        Usuario usuario = new Usuario();
        model.addAttribute("usuario", usuario);
        rutas(model);
        return "unauthenticated/login";
    }

    @PostMapping("/index/login")
    public String login(@Valid Usuario usuario, BindingResult result, Model model, RedirectAttributes flash, SessionStatus status) {
        if (result.hasErrors()) {
            return "unauthenticated/login";
        }
        HttpEntity<Usuario> entity = new HttpEntity<>(usuario);
        Object response = restTemplate.postForObject(url, usuario, Object.class );
        Usuario[] usuarioEncontrado = modelMapper.map(response, Usuario[].class);
        if(usuarioEncontrado.length > 0) {
            Empleado[] empleado = modelMapper.map(restTemplate.getForObject("http://localhost:58551/api/empleados7/" + usuarioEncontrado[0].getFk_Empleado(), Object.class), Empleado[].class);
            model.addAttribute("nombre", empleado[0].getNombre());
            return "redirect:/app/home";
        }else {
            flash.addFlashAttribute("error", "Usuario o contraseña inválido");
            return "redirect:/index/login";
        }
    }

}
