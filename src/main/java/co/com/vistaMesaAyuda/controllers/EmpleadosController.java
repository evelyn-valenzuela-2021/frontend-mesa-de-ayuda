package co.com.vistaMesaAyuda.controllers;

import co.com.vistaMesaAyuda.dtos.Area;
import co.com.vistaMesaAyuda.dtos.Empleado;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.net.URI;
import java.util.Objects;

import static org.springframework.http.HttpMethod.DELETE;

@Controller()
@RequestMapping("/app")
@SessionAttributes("empleado")
public class EmpleadosController {

    private final ModelMapper modelMapper;
    private final RestTemplate restTemplate;
    private final String url = "http://localhost:58551/api/empleados7/";
    private final String redListar = "redirect:/app/listar";

    public EmpleadosController(ModelMapper modelMapper, RestTemplate restTemplate) {
        this.modelMapper = modelMapper;
        this.restTemplate = restTemplate;
    }

    @ModelAttribute("findOne")
    public Empleado findById(String id) {
        Empleado[] empleados = modelMapper.map(restTemplate.getForObject(url + id, Object.class), Empleado[].class);
       return  empleados.length > 0 ? empleados[0] : null;
    }

    @ModelAttribute("empleados")
    public Empleado[] all() {
        return  modelMapper.map(restTemplate.getForObject(url, Object.class), Empleado[].class);
    }

    @ModelAttribute("areas")
    public Area[]  areas() {
        return modelMapper.map(restTemplate.getForObject(URI.create("http://localhost:57086/api/areas"), Object.class), Area[].class);
    }

    @ModelAttribute("rutas")
    public Model rutas(Model model) {
        model.addAttribute("ruta", "/app/listar");
        model.addAttribute("nombreRuta", "Empleados");
        model.addAttribute("ruta2", "/app/home");
        model.addAttribute("nombreRuta2", "Home");
        return model;
    }
    @GetMapping("/home")
    public String home(Model model) {
        rutas(model);
        return "authenticated/home";
    }

    @GetMapping("/listar")
    public String findAll(Model model) {
        rutas(model);
        model.addAttribute("titulo", "Listado de empleados");
        model.addAttribute("empleados", all());
        model.addAttribute("areas", areas());
        return "authenticated/list-employees";
    }

    @GetMapping("/form")
    public String put(@RequestParam(required = false) String id, @RequestParam(required = false) boolean isNew,  Model model, RedirectAttributes flash) {
        rutas(model);
        Empleado empleado = id != null ? findById(id) : new Empleado();

        if(Objects.isNull(empleado) && !isNew) {
            return redListar;
        }
        model.addAttribute("empleado", empleado);
        return "authenticated/form-employees";

    }

    @PostMapping("/form")
    public String save(@RequestParam(required = false) boolean isNew, @Valid Empleado empleado, BindingResult result, Model model, RedirectAttributes flash, SessionStatus status) {
        if (result.hasErrors()) {
            return "authenticated/form-employees";
        }
        if(isNew) {
            try {
                restTemplate.postForEntity(url, empleado, Object.class);

            } catch(HttpClientErrorException ex) {
                flash.addFlashAttribute("error", "No es posible guardar!, es probable que ya exista un usuario con el id ingresado");
                return "redirect:/app/form";
            }
        } else {
            restTemplate.put(url + empleado.getIdEmple(),empleado);
        }
        status.setComplete();
        flash.addFlashAttribute("success", "Empleado guardado con éxito");
        return redListar;

    }

    @GetMapping("/eliminar/{id}")
    public String delete(@PathVariable String id, Model model, RedirectAttributes flash) {
        Empleado empleado = findById(id);
        if(empleado == null) {
            flash.addFlashAttribute("error", "El empleado no existe en la base de datos");
            return redListar;
        }
        try {
            HttpEntity<Empleado> entity = new HttpEntity<>(empleado);
            restTemplate.exchange(url, DELETE, entity, Object.class);
            flash.addFlashAttribute("success", "Empleado eliminado con éxito");
        }catch (HttpClientErrorException exception) {
            flash.addFlashAttribute("error", "El empleado no puede ser eliminado");
        }
        return redListar;
    }

}
