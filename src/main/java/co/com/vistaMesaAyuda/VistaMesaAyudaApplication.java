package co.com.vistaMesaAyuda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VistaMesaAyudaApplication {

    public static void main(String[] args) {
        SpringApplication.run(VistaMesaAyudaApplication.class, args);
    }

}
