package co.com.vistaMesaAyuda.dtos;

import lombok.Data;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import static org.springframework.format.annotation.NumberFormat.Style.NUMBER;

@Data
public class Empleado {

    @NotBlank
    private String IdEmple;

    @NotBlank
    private String Nombre;

    @NotBlank
    @Size(min=7, max = 10)
    @NumberFormat(style = NUMBER)
    private String Tel;

    @Email
    @NotBlank
    private String Ema;

    @NotBlank
    private String Dir;

    @NumberFormat(style = NUMBER)
    private double X;

    @NumberFormat(style = NUMBER)
    private double Y;

    @NotBlank
    private String FkArea;

    private String FkEmple_Jefe;
    private String Foto;
    private String HojaVida;

    public String getIdEmple() {
        return IdEmple;
    }

    public void setIdEmple(String idEmple) {
        IdEmple = idEmple;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getTel() {
        return Tel;
    }

    public void setTel(String tel) {
        Tel = tel;
    }

    public String getEma() {
        return Ema;
    }

    public void setEma(String ema) {
        Ema = ema;
    }

    public String getDir() {
        return Dir;
    }

    public void setDir(String dir) {
        Dir = dir;
    }

    public double getX() {
        return X;
    }

    public void setX(double x) {
        X = x;
    }

    public double getY() {
        return Y;
    }

    public void setY(double y) {
        Y = y;
    }

    public String getFkArea() {
        return FkArea;
    }

    public void setFkArea(String fkArea) {
        FkArea = fkArea;
    }

    public String getFkEmple_Jefe() {
        return FkEmple_Jefe;
    }

    public void setFkEmple_Jefe(String fkEmple_Jefe) {
        FkEmple_Jefe = fkEmple_Jefe;
    }

    public String getFoto() {
        return Foto;
    }

    public void setFoto(String foto) {
        Foto = foto;
    }

    public String getHojaVida() {
        return HojaVida;
    }

    public void setHojaVida(String hojaVida) {
        HojaVida = hojaVida;
    }
}
