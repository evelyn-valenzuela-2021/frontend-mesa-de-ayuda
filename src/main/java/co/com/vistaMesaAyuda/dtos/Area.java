package co.com.vistaMesaAyuda.dtos;

import lombok.Data;

@Data
public class Area {

    private String IdArea;
    private String Nombre;
    private String FkEmple;

    public String getIdArea() {
        return IdArea;
    }

    public void setIdArea(String idArea) {
        IdArea = idArea;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getFkEmple() {
        return FkEmple;
    }

    public void setFkEmple(String fkEmple) {
        FkEmple = fkEmple;
    }
}
