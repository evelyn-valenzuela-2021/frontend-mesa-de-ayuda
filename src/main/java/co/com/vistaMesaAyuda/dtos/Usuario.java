package co.com.vistaMesaAyuda.dtos;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
public class Usuario {

    private int IdUsuario;

    @Email
    @NotBlank
    private String Correo;

    @NotBlank
    private String Contrasena;

    private String Fk_Empleado;

    public int getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        IdUsuario = idUsuario;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String correo) {
        Correo = correo;
    }

    public String getContrasena() {
        return Contrasena;
    }

    public void setContrasena(String contrasena) {
        Contrasena = contrasena;
    }

    public String getFk_Empleado() {
        return Fk_Empleado;
    }

    public void setFk_Empleado(String fk_Empleado) {
        Fk_Empleado = fk_Empleado;
    }
}
